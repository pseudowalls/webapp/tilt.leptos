use leptos::*;
use slice_group_by::GroupBy;
use num::ToPrimitive;
use pseudo_tilt::chern_character::{ChernChar, Δ};
use pseudo_tilt::tilt_stability::{wall, left_pseudo_semistabilizers};

const M: u32 = 2;

/// Record for a semistabilizer, along with reactive states for the UI
struct SVGcircleRecord {
    semistabs: Vec<ChernChar<M>>,
    centre: f64,
    radius: f64,
    focused: RwSignal<bool>,
    selected: RwSignal<bool>,
    index: usize,
}

/// Convert list of Chern characters `semistabilizers` of pseudosemistabilizers
/// for `v` into a list of records relating to associated circular walls,
/// including signals relating the the UI (to communicate events).
/// Records ordered in decreasing order of circle sizes.
fn circle_ui_data(semistabilizers: Vec<ChernChar<M>>, v: &ChernChar<M>) -> Vec<SVGcircleRecord> {
    let mut semistabs_with_radii = semistabilizers
        .into_iter()
        .map(|semistab|
             (
                 semistab,
                 wall::radius2(&semistab, v)
                     // TODO should probably error out here instead
                     .unwrap_or(0.into())
             )
        )
        .collect::<Vec<_>>();

    // reorder in decreasing size (hence increasing centres)
    // TODO: revisit potential performance regression here
    semistabs_with_radii
        .sort_by_key(|u| u.1);
    semistabs_with_radii.reverse();

    semistabs_with_radii
        // group Chern chars by equal radii
        .linear_group_by_key(|x| x.1)
        .enumerate()
        .map(|(index, group)| {
            let radius2 = group[0].1; // group_by should not give any empty group
            let semistab = group[0].0; // keep representative to calculate wall radius
            let semistabs = group
                .iter()
                .map(|pair| pair.0)
                .collect::<Vec<_>>();

            SVGcircleRecord {
                semistabs,
                centre: wall::centre(&semistab, v)
                    .unwrap_or(0.into())
                    .to_f64()
                    .unwrap_or(0.),
                radius: radius2
                    .to_f64()
                    .unwrap_or(0.)
                    .sqrt(),
                focused: create_rw_signal(false),
                selected: create_rw_signal(false),
                index,
            }
        })
        .collect()
}

#[component]
pub fn ChernReport(chern: ReadSignal<ChernChar<M>>) -> impl IntoView {
    // TODO use some async rust here to have a loading notification
    let fully_rerendering_section = move || view!{
        <DiagramWithSidebar chern={chern.get()} />
    };
    view! {
        <ChernDetails chern />
        {fully_rerendering_section}
    }
}

#[component]
pub fn DiagramWithSidebar(chern: ChernChar<M>) -> impl IntoView {
    let v = chern;

    match left_pseudo_semistabilizers::find_all::<M>(&v)
        // Process semistabilizers (if no error) into entries relating to UI
        .map(|ss| circle_ui_data(ss, &v))
    {
        Ok(cr) => view! {
            <div class="container">
                <Diagram circle_records={&cr} _v={v} />
                <SemistabList circle_records={&cr} />
            </div>
        },
        Err(e) => view! {
            <div>
                <p>
                "error: "
                {move || e.to_string()}
                </p>
            </div>
        },
    }
}

#[component]
pub fn ChernDetails(chern: ReadSignal<ChernChar<M>>) -> impl IntoView {
    view! {
        <p>
          "Pseudowalls for Chern character: "
          <math display="inline-block"
            style="display:inline-block math;">
            <mrow>
              <mo fence="true" form="prefix">"("</mo>
              <mn>{move || chern.get().r}</mn>
              <mo separator="true">","</mo>
              <mn>{move || i64::from(chern.get().c)}</mn>
              <mi>"ℓ"</mi>
              <mo separator="true">","</mo>
              <mn>{move || chern.get().d.raw_int_repn()}</mn>
              <mo fence="true" form="postfix">")"</mo>
            </mrow>
          </math>
          ", with: "
          <math display="inline-block"
            style="display:inline-block math;">
            <mrow>
              <mrow>
                <mi mathvariant="normal">Δ</mi>
              </mrow>
              <mo>=</mo>
              <mn>{move || Δ(&chern.get()).ℓ2_coeff().to_string()}</mn>
              <msup>
                <mi>ℓ</mi>
                <mn>2</mn>
              </msup>
              <mo separator="true">,</mo>
              <mi>μ</mi>
              <mo>=</mo>
              <mn>{move || i64::from(chern.get().c)}</mn>
              <mo>/</mo>
              <mn>{move || chern.get().r}</mn>
              <msup>
                <mi>ℓ</mi>
                <mn>2</mn>
              </msup>
            </mrow>
          </math>
        </p>
    }
}

#[component]
fn Diagram<'a>(circle_records: &'a Vec<SVGcircleRecord>, _v: ChernChar<M>) -> impl IntoView {

    struct Circ {
        radius: f64,
        centre: f64
    }

    let biggest_circle: Circ = match circle_records.get(0) {
        Some(circ_record) => Circ {
            radius: circ_record.radius,
            centre: circ_record.centre,
        },
        // default to ficticious "largest circle" in case of no walls:
        None => Circ {
            radius: 1.,
            centre: 0.,
        },
    };

    let bmax: f64 = biggest_circle.centre + biggest_circle.radius;

    let bmin: f64 = biggest_circle.centre - biggest_circle.radius;

    let ymax: f64 = biggest_circle.radius;

    let circles_view = circle_records
        .iter()
        .map(|c| {
            let focused = c.focused;
            let selected = c.selected;

            view! {
                <circle
                    cx={c.centre.to_string()}
                    cy={ymax}
                    r={c.radius.to_string()}
                    class:infocus={focused}
                    class:selected={selected}
                    // use arbitrary proportion of radius for stroke width
                    stroke-width={(c.radius/100.).to_string()}
                    on:mouseenter={move |_| {
                        focused.set(true);
                    }}
                    on:mouseleave={move |_| {
                        focused.set(false);
                    }}
                    on:click={move |_| {
                        selected.update(|old| (*old = !*old));
                        focused.set(false);
                    }}
                />
            }
        })
        .collect_view();

    view! {
        <div class="svg-container">
        <svg
            viewBox={format!("{} 0 {} {}", bmin, bmax-bmin, ymax)}
        >
            {circles_view}
        </svg>
        </div>
    }
}

#[component]
fn WallSemistabs<'a>(semistabilizers: &'a Vec<ChernChar<M>>) -> impl IntoView {
    let list_entries = semistabilizers
        .iter()
        .map(|chern|
             view! {
                 <li>{chern.to_string()}</li>
             }
        )
        .collect_view();

    view! {
        <ul>
        {list_entries}
        </ul>
    }
}

#[component]
fn Tab<'a>(c: &'a SVGcircleRecord) -> impl IntoView {
    let focused = c.focused;
    let selected = c.selected;
    let index = c.index;

    view! {
        <div
            class="wall-destabilisers"
            class:infocus={focused}
            class:selected={selected}
            // details to communicate to css:
            style={format!("--n: {}; --nstr: '{}'", index%10, index+1)}
            on:mouseenter={move |_| {
                focused.set(true);
            }}
            on:mouseleave={move |_| {
                focused.set(false);
            }}
            on:click={move |_| {
                selected.update(|old| (*old = !*old));
                focused.set(false);
            }}
        >
            <div>
            <WallSemistabs semistabilizers={&c.semistabs} />
            </div>
        </div>
    }
}

#[component]
fn SemistabList<'a>(circle_records: &'a Vec<SVGcircleRecord>) -> impl IntoView {
    let circle_records_chunks = circle_records
        .linear_group_by_key(|c| c.index/10);
    let most_wall_file_tabs = circle_records_chunks
        .map(|chunk| {
            let tab_group = chunk
                .iter()
                .map(|c| {
                    view! { <Tab c={c} /> }
                })
                .collect_view();
            view! {
                <div
                    class="wall-destabilisers-container"
                    style="--numwalls: 10">
                    {tab_group}
                </div>
            }
        })
        .collect_view();

    view! {
        <div class="sidebar">
            <h1>Sidebar</h1>
            {most_wall_file_tabs}
        </div>
    }
}
