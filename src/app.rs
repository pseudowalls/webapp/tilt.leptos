mod chern_report;
mod chern_input;

use chern_report::ChernReport;
use chern_input::ChernInput;
use leptos::*;
use pseudo_tilt::chern_character::ChernChar;
use leptos::*;
use leptos_meta::*;
use leptos_router::*;

#[component]
pub fn App() -> impl IntoView {
    // Provides context that manages stylesheets, titles, meta tags, etc.
    provide_meta_context();

    view! {
        // injects a stylesheet into the document <head>
        // id=leptos means cargo-leptos will hot-reload this stylesheet
        <Stylesheet id="leptos" href="/pkg/tilt-ssr-test.css"/>

        // sets the document title
        <Title text="Pseudowalls App"/>

        // content for this welcome page
        <Router>
            <main>
                <Routes>
                    <StaticRoute
                    mode=StaticMode::Upfront
                    path=""
                    view=Page
                    static_params=move || Box::pin(async move {
                        StaticParamsMap::default()
                    })
                    />
                </Routes>
            </main>
        </Router>
    }
}

const M: u32 = 2;

#[island]
fn WallsUI() -> impl IntoView {
    let (v, set_v) = create_signal(ChernChar::<M>::from((3, 2, -4)));
    return view! {
        <ChernInput chern={set_v}/>
        <ChernReport chern={v}/>
    };
}

#[component]
fn Page() -> impl IntoView {
    return view! {
        <Instructions />
        <WallsUI />
    };
}

#[component]
fn Instructions() -> impl IntoView {
    return view! {
        <p>
        "Choose a positive ranked Chern character (v) for an object in
        Dᵇ(X) where X is a principally polarized surface who's
        polarization has c₁=ℓ. If the bogomolov discriminant
        (Δ(v)) is a square integer times ℓ², then there are finitely
        many walls for v and a finite superset (pseudowalls) will be
        computed below after submission."
        </p>
    };
}
