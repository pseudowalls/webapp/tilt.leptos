use leptos::*;
use pseudo_tilt::chern_character::{ChernChar, Δ};
use pseudo_tilt::tilt_stability::β_;

const P2: u32 = 2;

#[component]
pub fn ChernInput(chern: WriteSignal<ChernChar<P2>>) -> impl IntoView {
    const DEFAULT_R: i64 = 3;
    const DEFAULT_C: i64 = 2;
    const DEFAULT_D: i64 = -4;

    let (r, set_r) = create_signal(DEFAULT_R);
    let (c, set_c) = create_signal(DEFAULT_C);
    let (d, set_d) = create_signal(DEFAULT_D);

    let staging_chern = move || ChernChar::<P2>::from((r.get(), c.get(), d.get()));

    // Derived signals to flag issues with input
    let rank_is_negative = move || r.get() < 0;
    let bgmlv_is_not_square = move || β_(&staging_chern()).is_none();

    view! {
        <div class="chern_input">
            <math
                display="inline-block"
                class="tml-display"
                style="display:block math;">
              <mtable displaystyle="true"
                columnalign="right left">
              <mtr>
              <mtd class="tml-right"
                style="padding:0.7ex 0em 0.7ex 0em;">
              <mrow>
                <mi>"ch"</mi>
                <mo form="prefix" stretchy="false">"("</mo>
                <mi>"v"</mi>
                <mo form="postfix" stretchy="false">")"</mo>
              </mrow>
              </mtd>
              <mtd class="tml-left"
                style="padding:0.7ex 0em 0.7ex 0em;">
              <mrow>
                <mo>"="</mo>
                <mo form="prefix" stretchy="true">"("</mo>
                <mtable>
                  <mtr>
                    <mtd>
                        <button
                            on:click=move |_|
                            {set_r.update(|r| {*r+=1;});}
                        >"+"</button>
                    </mtd>
                    <mtd>
                        <button
                            on:click=move |_|
                            {set_c.update(|c| {*c+=1;});}
                        >"+"</button>
                    </mtd>
                    <mtd>
                        <button
                            on:click=move |_|
                            {set_d.update(|d| {*d+=1;});}
                        >"+"</button>
                    </mtd>
                  </mtr>
                  <mtr>
                    <mtd>
                    <mn class:invalid={rank_is_negative}>{r}</mn>
                    ","
                    </mtd>
                    <mtd><mn>{c}"ℓ,"</mn></mtd>
                    <mtd><mn>{d}</mn></mtd>
                  </mtr>
                  <mtr>
                    <mtd>
                        <button
                            on:click=move |_|
                            {set_r.update(|r| {*r-=1;});}
                        >"−"</button>
                    </mtd>
                    <mtd>
                        <button
                            on:click=move |_|
                            {set_c.update(|c| {*c-=1;});}
                        >"−"</button>
                    </mtd>
                    <mtd>
                        <button
                            on:click=move |_|
                            {set_d.update(|d| {*d-=1;});}
                        >"−"</button>
                    </mtd>
                  </mtr>
                </mtable>
                <mo form="postfix" stretchy="true">")"</mo>
              </mrow>
              </mtd>
              </mtr>
              <mtr
                class:invalid={bgmlv_is_not_square}>
              <mtd class="tml-right"
                style="padding:0.7ex 0em 0.7ex 0em;">
                <mrow>
                  <mrow>
                    <mi mathvariant="normal">"Δ"</mi>
                  </mrow>
                  <mo form="prefix" stretchy="false">"("</mo>
                  <mi>"v"</mi>
                  <mo form="postfix" stretchy="false">")"</mo>
                </mrow>
              </mtd>
              <mtd class="tml-left"
                style="padding:0.7ex 0em 0.7ex 0em;">
                <mrow>
                  <mo>"="</mo>
                  <mn>
                      { move  || Δ(&staging_chern()).ℓ2_coeff().to_string() }
                  </mn>
                  <msup>
                    <mi>ℓ</mi>
                    <mn>2</mn>
                  </msup>
                </mrow>
                <button
                  class="submit"
                  on:click=move |_| {chern.set(staging_chern())}>
                  "submit"
                </button>
              </mtd>
              </mtr>
              </mtable>
            </math>
        </div>
    }
}
